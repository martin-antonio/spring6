package com.martin.ioc;

import com.martin.bean.UserDao;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test01 {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        // 1.根据ID获取bean
        User user = (User) context.getBean("user");

        System.out.println(user);
        //2.根据类型获取bean
        User user1 = context.getBean(User.class);

        System.out.println(user1);

        //3.根据ID和类型
        User user3 = context.getBean("user", User.class);

        System.out.println(user3);

    }

    @Test
    public void test02(){
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        UserDao bean = context.getBean(UserDao.class);
        bean.print();
    }
}
