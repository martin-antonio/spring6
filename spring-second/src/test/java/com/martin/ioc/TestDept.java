package com.martin.ioc;

import com.martin.diTest.Dept;
import com.martin.diTest.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDept {

    @Test
    public void test01() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        Employee emp = context.getBean("emp", Employee.class);
        emp.work();
        emp.toString();
    }

    @Test
    public void test02() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        Employee emp = context.getBean("emp2", Employee.class);
        emp.work();
    }

    @Test
    public void test03() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        Employee emp = context.getBean("emp3", Employee.class);
        emp.work();
    }

    @Test
    public void test04() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean2.xml");
        Employee emp = context.getBean("emp2", Employee.class);
        emp.work();
        emp.toString();
    }

    @Test
    public void test05() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean2.xml");
        Dept emp = context.getBean("dept4", Dept.class);
        emp.info();
        for (Employee employee : emp.getEmployeeList()) {
            System.out.println(employee.getEname());
        }
        emp.toString();
    }
}
