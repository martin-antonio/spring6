package com.martin.ioc;

import com.alibaba.druid.pool.DruidDataSource;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestJDBC {

    @Test
    public void TestDruid(){
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/spring?serverTimezone=UTC");
        dataSource.setUsername("root");
        dataSource.setPassword("1234");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");

    }

    @Test
    public void TestDruid2(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean-jdbc.xml");
        DruidDataSource dataSource = applicationContext.getBean(DruidDataSource.class);
//        DruidDataSource dataSource = new DruidDataSource();
//        dataSource.setUrl("jdbc:mysql://localhost:3306/spring?serverTimezone=UTC");
//        dataSource.setUsername("root");
//        dataSource.setPassword("1234");
//        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        System.out.println(dataSource.getUrl());

    }

}
