package com.martin.ioc;

import com.martin.diTest1.Student;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestStudent2 {
    @Test
    public void Test(){

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("sbean.xml");
        Student student = (Student)applicationContext.getBean("student");
        student.run();
    }
}
