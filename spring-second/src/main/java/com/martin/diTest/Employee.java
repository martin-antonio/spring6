package com.martin.diTest;

import java.util.Arrays;

public class Employee {
    // 员工属于部门
    private Dept dept;
    private String ename;
    private Integer age;
    public void work() {
        System.out.println(ename + "员工在工作," + age);
        this.dept.info();
    }

    @Override
    public String toString() {
        return "Employee{" +
                "dept=" + dept +
                ", ename='" + ename + '\'' +
                ", age=" + age +
                ", loves=" + Arrays.toString(loves) +
                '}';
    }

    private String[] loves;

    public String[] getLoves() {
        return loves;
    }

    public void setLoves(String[] loves) {
        this.loves = loves;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
