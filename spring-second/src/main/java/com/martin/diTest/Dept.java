package com.martin.diTest;

import java.util.List;

public class Dept {
    private String dname;

    private List<Employee> employeeList;
    public void info(){

        System.out.println("部门名称: "+dname);

    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "dname='" + dname + '\'' +
                ", employeeList=" + employeeList +
                '}';
    }

}
