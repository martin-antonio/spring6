package com.martin.spring;/*
 * @Author Martin·Antonio
 * @Description TODO
 * @DateTime 2024/5/9 19:49
 * */

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.lang.reflect.InvocationTargetException;

public class Test0 {
    private Logger logger  = LoggerFactory.getLogger(Test0.class);


    @Test
    public void testUserObject(){
        //创建对象
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        //获取创建对象
        User user = (User) applicationContext.getBean("user");

        user.add();

        System.out.println(user);

        logger.info("测试spring6日志");

    }
    @Test
    public void testUserObject1() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Class clazz= Class.forName("com.martin.spring.User");

        User user = (User) clazz.getDeclaredConstructor().newInstance();

        System.out.println(user);

    }
}
