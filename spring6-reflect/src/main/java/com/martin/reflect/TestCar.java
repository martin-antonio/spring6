package com.martin.reflect;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class TestCar {

    //1.获取class的多种方式
    @Test
    public void test01() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class carClass = Car.class;
        carClass.getClass();
        Class<? extends Class> aClass = Car.class.getClass();

        Class<?> aClass1 = Class.forName("com.martin.reflect.Car");

        Car o = (Car) aClass1.getDeclaredConstructor().newInstance();
    }

    //2.获取构造
    @Test
    public void test02() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class clazz = Car.class;
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println(constructor.getName() + constructor.getParameterCount());
        }
        Constructor constructor = clazz.getConstructor(String.class, int.class, String.class);
        Car car = (Car) constructor.newInstance("夏利", 10, "11111");

        Constructor constructor2 = clazz.getConstructor(String.class, int.class, String.class);
        constructor2.setAccessible(true);
        Car car2 = (Car) constructor2.newInstance("夏利", 10, "11111");

    }
    //3.获取属性
    @Test
    public void test03() throws Exception{
        Class clazz = Car.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.println(field.getName());
        }
    }

}
